import json
import os
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, mixins

from django.contrib.auth.models import User
from django.conf import settings
from status.models import Status, ImageEncodings
from .serializers import StatusSerializer
from django.shortcuts import get_object_or_404
from .utils import fr_util


# class StatusListSearchAPIView(APIView):
#     permission_classes = []
#     authentication_classes = []
#
#     def get(self, request, format=None):
#         qs = Status.objects.all()
#         serializer = StatusSerializer(qs, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         qs = Status.objects.all()
#         serializer = StatusSerializer(qs, many=True)
#         return Response(serializer.data)
class StatusAPIDetailView(
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.RetrieveAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Status.objects.all()
    serializer_class = StatusSerializer
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class StatusAPIView(mixins.CreateModelMixin,
                    generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    # queryset = Status.objects.all()
    serializer_class = StatusSerializer

    def get_queryset(self):
        qs = Status.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs


    def get(self, request, *args, **kwargs):
        url_passed_id = request.GET.get('id', None)
        # json_data = json.loads(request.body)
        # new_passed_id = json_data.get('id', None)
        # passed_id = url_passed_id or new_passed_id or None
        passed_id = url_passed_id
        # queryset = self.get_queryset()
        if passed_id is not None:
            return self.retrieve(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # import ipdb; ipdb.set_trace()
        uploaded = request.FILES['image']
        st = Status(user=User.objects.all().first(), image=uploaded )
        st.save()
        # filename = st.image.name
        # filepath = os.path.dirname(os.path.abspath(__file__))
        # imageFile = request.data['image'].read()
        # uploaded = request.FILES['image']
        # from django.core.files.base import ContentFile
        # file_content = ContentFile(uploaded.read())
        # new_file = Status()
        #1 - get it into the DB and note system so we know the real path
        # new_file.image.save(str(new_file.id) + '.jpg', file_content)
        # new_file.save()
        # import ipdb; ipdb.set_trace()
        with open(st.image.file.name, 'rb') as img:
            val = fr_util.encImg(img).tolist()
            # import ipdb; ipdb.set_trace()
            ie = ImageEncodings(encoding=val)
            ie.save()
        return self.create(request, *args, **kwargs)

    def get_object(self):
        request = self.request
        passed_id = request.GET.get('id', None)
        queryset = self.get_queryset()
        obj = None
        if passed_id is not None:
            obj = get_object_or_404(queryset, id=passed_id)
            self.check_object_permissions(request, obj)
        return obj
    #
    # def perform_destroy(self, instance):
    #     if instance is not None:
    #         return instance.delete()
    #     return None
    #
    # def put(self, request, *args, **kwargs):
    #     return self.update(request, *args, **kwargs)
    #
    # def patch(self, request, *args, **kwargs):
    #     return self.patch(request, *args, **kwargs)
    #
    # def delete(self, request, *args, **kwargs):
    #     return self.destroy(request, *args, **kwargs)
# class StatusCreateAPIView(generics.CreateAPIView):
#     permission_classes = []
#     authentication_classes = []
#     queryset = Status.objects.all()
#     serializer_class = StatusSerializer

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)

# class StatusDetailAPIView(mixins.DestroyModelMixin,mixins.UpdateModelMixin, generics.RetrieveAPIView):
#     permission_classes = []
#     authentication_classes = []
#     queryset = Status.objects.all()
#     serializer_class = StatusSerializer
#     # lookup_field = 'id'
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)
