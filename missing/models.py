from django.db import models
from django.contrib.postgres.fields import ArrayField
# Create your models here.

class MissingPerson(models.Model):
    image = models.ImageField(upload_to='missing')
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lon = models.DecimalField(max_digits=9, decimal_places=6)
    email = models.EmailField(max_length=70,blank=True, null= True, unique= True)
    image_encoding = ArrayField(models.DecimalField(max_digits=20, decimal_places=15))
    image_found = models.BooleanField(default=False, blank=True)
    first_name = models.CharField(max_length = 250, blank=True)
    last_name = models.CharField(max_length = 250, blank=True)

    def __str__(self):
        return f"{self.lat} {self.lon}"
