import face_recognition


def encImg(*args):
    #Get the first image
    # known_image = face_recognition.load_image_file("/Users/yshthdn/Desktop/Development/my_env/opencv/images/justin/1.jpg")
    #Get the second image
    # unknown_image = face_recognition.load_image_file("/Users/yshthdn/Desktop/Development/my_env/opencv/images/kit-harington/1.jpg")
    load_img = face_recognition.load_image_file(args[0])
    #use encodings on both images
    # biden_encoding = face_recognition.face_encodings(known_image)[0]
    # unknown_encoding = face_recognition.face_encodings(unknown_image)[0]
    gen_encoding = face_recognition.face_encodings(load_img)[0]
    return gen_encoding
    #Compare the two faces based ont those encodings
    # results = face_recognition.compare_faces([biden_encoding], unknown_encoding)

    #Prints true if those persons(faces) are same else prints false
    # print(results)
