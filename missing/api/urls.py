from django.conf.urls import url
from .views import (
                    MissingPersonAPIView,
                    MissingPersonAPIDetailView,
            
                    )

urlpatterns = [
    url(r'^$', MissingPersonAPIView.as_view()),

    url(r'^(?P<pk>.*)/$', MissingPersonAPIDetailView.as_view()),
]
