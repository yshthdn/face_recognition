# face_recognition

USAGE:
1. To be used when user profile gets created
POST, GET
<hostname>/api/user_profile

media_type: multipart/form-data

{
    "first_name": "",
    "last_name": "",
    "image": null,
    "phone_number": ""
}

PUT, PATCH, DELETE
<hostname>/api/user_profile/<id>


2. To be used to check for missing person
POST, GET
<hostname>/api/missing

media_type: multipart/form-data

{
    "image": null,
    "lat": null,
    "lon": null,
    "email": "",
    "image_encoding": [],
    "image_found": false,
    "first_name": "",
    "last_name": ""
}

PUT, PATCH, DELETE
<hostname>/api/missing/<id>