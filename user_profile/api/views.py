from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserProfileModelSerializer
from user_profile.models import UserProfileModel
from .utils import fr_util

class UserProfileAPIView(mixins.CreateModelMixin,
                           generics.ListAPIView,
                           ):
    permission_classes = []
    authentication_classes = []
    serializer_class = UserProfileModelSerializer

    def get_queryset(self):
        qs = UserProfileModel.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def get(self, request, *args, **kwargs):
        url_passed_id = request.GET.get('id', None)
        passed_id = url_passed_id
        if passed_id is not None:
            return self.retrieve(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = UserProfileModelSerializer(data=request.data)
        if serializer.is_valid():
            uploaded = request.FILES['image']
            # import ipdb; ipdb.set_trace()
            phone =  request.data.get('phone_number',None)
            first_name = request.data.get('first_name',None)
            last_name = request.data.get('last_name',None)

            st = UserProfileModel(image=uploaded, phone_number=phone,first_name=first_name, last_name=last_name, image_encoding=[0.0])
            st.save()
            key = st.id
            obj = None
            with open(st.image.file.name, 'rb') as img:
                val = fr_util.encImg(img).tolist()
                # import ipdb; ipdb.set_trace()
                obj = UserProfileModel.objects.filter(id=key)
                obj.update(image_encoding=val)

            return Response(status=201)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self):
        request = self.request
        passed_id = request.GET.get('id', None)
        queryset = self.get_queryset()
        obj = None
        if passed_id is not None:
            obj = get_object_or_404(queryset, id=passed_id)
            self.check_object_permissions(request, obj)
        return obj

class UserProfileAPIDetailView(mixins.UpdateModelMixin,
                                mixins.DestroyModelMixin,
                                generics.RetrieveAPIView):

    permission_classes = []
    authentication_classes = []
    queryset = UserProfileModel.objects.all()
    serializer_class = UserProfileModelSerializer


    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
