from django.conf.urls import url
from .views import (
                    UserProfileAPIView,
                    UserProfileAPIDetailView,

                    )

urlpatterns = [
    url(r'^$', UserProfileAPIView.as_view()),

    url(r'^(?P<pk>.*)/$', UserProfileAPIDetailView.as_view()),
]
