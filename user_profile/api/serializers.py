from rest_framework import serializers
from user_profile.models import UserProfileModel

class UserProfileModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfileModel
        fields = ['id','first_name','last_name','image', 'phone_number']
